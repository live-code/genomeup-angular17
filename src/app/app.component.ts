import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, NavbarComponent],
  template: `
     <app-navbar />
     <div class="p-6">
     <router-outlet></router-outlet>
     </div>
  `,
  styles: [],
})
export class AppComponent {
  title = 'angular-genomeup17';
}
