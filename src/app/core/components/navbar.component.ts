import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `
    <button routerLink="demo1">demo1</button>
    <button routerLink="demo2">demo2</button>
    <button routerLink="demo3">demo3</button>
    <button routerLink="demo4">demo4</button>
    <button routerLink="demo5">demo5</button>
    <button routerLink="demo6">demo6</button>
    <button routerLink="demo7">demo7</button>
    <button routerLink="demo8">demo8</button>
    <button routerLink="demo9">demo9</button>
    <button routerLink="demo10">demo10</button>
    <button routerLink="demo11">demo11</button>
    <button routerLink="demo12">demo12</button>
    <button routerLink="demo13">demo13</button>
    <button routerLink="demo14">demo14</button>
  `,
  styles: ``
})
export class NavbarComponent {

}
