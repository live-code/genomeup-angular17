import { Component } from '@angular/core';
import { BoxComponent } from '../shared/box.component';

@Component({
  selector: 'app-demo13',
  standalone: true,
  imports: [
    BoxComponent
  ],
  template: `
    <p>
      demo13 works!
    </p>
    
    <app-box title="box 1"/>
    <app-box 
      title="box 2"
      variant="warn" 
      size="xl"
      body="lorem..."
      [value]="5"
      [items]="['a','fa' ,'fabio']"
      zoom="10"
      showTitle
    />
    
  `,
  styles: ``
})
export default  class Demo13Component {

}
