import { HttpClient } from '@angular/common/http';
import { Component, inject, OnInit, signal } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, filter, mergeMap, of, switchMap } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [ReactiveFormsModule],
  template: `
      <h1>Meteo con RxJS / imperative / signal</h1>
      
      <input type="text" placeholder="Search City" [formControl]="input">

      {{ meteo?.main?.temp }}°
      {{ meteoSignal()?.main?.temp }}°
  `,
  styles: ``
})
export default class Demo6Component implements OnInit {
  input = new FormControl('', { nonNullable: true })
  http = inject(HttpClient)
  meteo: Meteo | null = null;
  meteoSignal = signal<Meteo | null>(null)

  ngOnInit() {
    this.input.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        switchMap(
          text => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(() => of(null))
            )
        )
      )
      .subscribe(meteo => {
        this.meteo = meteo;
        this.meteoSignal.set(meteo)
      })
  }
}
