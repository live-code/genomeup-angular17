import { Component, inject, Optional } from '@angular/core';
import { BaseService } from '../shared/services/base.service';
import { PostService } from '../shared/services/post.service';
import { UserService } from '../shared/services/user.service';
import { WidgetComponent } from '../shared/widget.component';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    WidgetComponent
  ],
  template: `
    <p>
      demo5 works!
    </p>
    
    <app-widget />
  `,
  providers: [
    { provide: BaseService, useClass: PostService}
  ],
  styles: ``
})
export default class Demo5Component {
  userService = inject(UserService, { optional: true })

  constructor() {
    if (this.userService) {
      // ...
    } else {
      // ...
    }
    console.log(this.userService)
  }
}
