import { AsyncPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject } from '@angular/core';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, filter, interval, of, switchMap } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-demo8',
  standalone: true,
  imports: [
    AsyncPipe,
    ReactiveFormsModule
  ],
  template: `
      <h1>Meteo con toSignal</h1>
      <input type="text" placeholder="Search City" [formControl]="input">

      {{ (meteoSignal())?.main?.temp }}°
  `,
  styles: ``
})
export default class Demo8Component {
  input = new FormControl('', { nonNullable: true })
  http = inject(HttpClient)

  meteoSignal = toSignal(
    this.input.valueChanges
      .pipe(
        // takeUntilDestroyed(),
        filter(text => text.length > 2),
        debounceTime(1000),
        switchMap(
          text => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(() => of(null))
            )
        )
      )
  )
}
