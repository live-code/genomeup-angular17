import { CommonModule, JsonPipe } from '@angular/common';
import { Component, computed, signal } from '@angular/core';

@Component({
  selector: 'app-demo10',
  standalone: true,
  imports: [JsonPipe, CommonModule],
  template: `
    @if(logged()) {
        <div class="bg"> Hi dev! </div>
        <button (click)="logout()">Logout</button>
    } @else {
        <h1>Login</h1>
        <button (click)="signIn()">SignIn</button>
    }
  `,
  styles: `
    .bg { background: red }
  `
})
export default class Demo10Component {
  logged = signal(false)
  isNotLogged = computed(() => !this.logged())

  signIn() {
      this.logged.set(true)
  }

  logout() {
    this.logged.set(false)
  }

}
