import { AsyncPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, filter, of, switchMap } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    AsyncPipe
  ],
  template: `
      <h1>Meteo con RxJS</h1>
      <input type="text" placeholder="Search City" [formControl]="input">

      {{ (meteo$ | async)?.main?.temp }}°
  `,
  styles: ``
})
export default class Demo7Component {
  input = new FormControl('', { nonNullable: true })
  http = inject(HttpClient)
  meteo$ = this.input.valueChanges
    .pipe(
      filter(text => text.length > 2),
      debounceTime(1000),
      switchMap(
        text => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .pipe(
            catchError(() => of(null))
          )
      )
    )

}
