import { JsonPipe } from '@angular/common';
import { Component, computed, signal } from '@angular/core';

type Product = {
  id: number;
  name: string;
  cost: number;
}

@Component({
  selector: 'app-demo12',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>for</h1>
    
    @for (product of products(); track product.id; let i = $index; let last = $last) {
        <li>{{i+1}} {{product.name}} - {{last}}</li>
    } @empty {
        <button (click)="load()">Load</button>
    }
    
    @if(!noItems()) {
      <div>
          Total: {{totalItems()}}
      </div>
    }
   
  `,
  styles: ``
})
export default  class Demo12Component {
  products = signal<Product[]>([]);
  noItems = computed(() => this.products().length === 0)
  totalItems = computed(() => this.products().length)

  load() {
    this.products.set([
      {id: 1, name: 'Chocolate', cost: 3},
      {id: 2, name: 'Milk', cost: 1},
      {id: 3, name: 'Biscuits', cost: 2},
    ])
  }
}
