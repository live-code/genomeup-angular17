import { Component, signal } from '@angular/core';
import { BigListComponent } from '../shared/big-list.component';
import { BoxComponent } from '../shared/box.component';
import { VspaceComponent } from '../shared/vspace.component';
import { WidgetComponent } from '../shared/widget.component';

@Component({
  selector: 'app-demo14',
  standalone: true,
  imports: [
    VspaceComponent,
    BoxComponent,
    WidgetComponent,
    BigListComponent
  ],
  template: `

      
      <h1>Demo 1 defer</h1>
      
      @defer (when isVisible()) {
          @if (isVisible()) {
              <app-box title="boxY" showTitle />
          } 
      } @placeholder {
          <button (click)="isVisible.set(true)">
              show
          </button>
      }

      
      <h1>Demo 2 interaction</h1>

      @defer (on interaction) {
          <app-box title="boxY" showTitle />
      } @placeholder {
          <button>show</button>
      }

      <h1>Demo 3 timer</h1>
      @defer (on timer(1000)) {
          <app-box title="boxY" showTitle />
      } @placeholder {
          loading box1...
      }
      @defer (on timer(2000)) {
          <app-box title="boxY" showTitle />
      } @placeholder {
          loading box2...
      }
      
      <h1>Demo 4 viewport</h1>

      <app-vspace height="1000"></app-vspace>
      @defer (on viewport) {
          <app-widget  />
      } @placeholder {
          <div>loading...</div>
      }
      
      <h1>Demo 5 big list / loading</h1>
      
      @defer (on interaction) {
          <app-big-list />
      } @loading( minimum 500ms)  {
          <div>rendering...</div>
      }
      @placeholder {
          <button>load big list</button>
      }

  `,
  styles: ``
})
export default  class Demo14Component {
  isVisible = signal(false)


}
