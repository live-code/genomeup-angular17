import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, signal } from '@angular/core';

type User = {
  id: number;
  name: string;
}

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [CommonModule],
  template: `
      <p>
          demo2 works!
      </p>

      <h1>Counter: {{ counter() }}</h1>

      <div *ngIf="isZero()">Counter is empty</div>

      <button (click)="inc()">+</button>
      <button (click)="dec()">-</button>
      <button (click)="reset()">reset</button>

      <h1>{{ user()?.name }}</h1>

  `,
  styles: ``
})
export default class Demo2Component {
  counter = signal(1)
  isZero = computed(() => this.counter() === 0)
  user = signal<User | null>(null)

  constructor(private http: HttpClient) {
    effect(() => {
      this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${this.counter()}`)
        .subscribe(res => {
          this.user.set(res)
        })
    });

    effect(() => {
      console.log(this.isZero())
    });
  }

  inc() {
    this.counter.set(this.counter() + 1)
  }

  dec() {
    this.counter.update(c => c - 1)
  }

  reset() {
    this.counter.set(1)
  }
}
