import { CommonModule, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, Input, OnInit, Signal, signal } from '@angular/core';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    JsonPipe, CommonModule
  ],
  template: `
      <h1>Todo List</h1>

      <div *ngIf="noTodos()">there are no todos</div>

      <input type="text" (keydown.enter)="addTodo($event)">

      <li *ngFor="let todo of todos(); let i = index">
          <input type="checkbox" [checked]="todo.completed" (change)="toggleTodo(todo, i)">
          {{ todo.title }}
          <button (click)="deleteTodo(todo)">❌</button>
      </li>

      <pre>{{todos() | json}}</pre>
      
  `,
  styles: ``
})
export default class Demo3Component implements OnInit {
  http = inject(HttpClient);
  todos = signal<Todo[]>([])
  noTodos = computed(() => this.todos().length === 0)

  ngOnInit() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .subscribe(result => {
        this.todos.set(result);
      })
  }

  deleteTodo(todo: Todo) {
    this.http.delete(`http://localhost:3000/todos/${todo.id}`)
      .subscribe(() => {
        this.todos.update((todos => todos.filter(t => t.id !== todo.id)))
      })
  }

  addTodo(event: Event) {
    const target = event.target as HTMLInputElement;

    this.http.post<Todo>('http://localhost:3000/todos', {
      title: target.value,
      completed: false
    })
      .subscribe(newTodo => {
        // this.todos.set([...this.todos(), newTodo])
        this.todos.update(todos => [...todos, newTodo])
        target.value = '';
      })


  }

  toggleTodo(todo: Todo, index: number) {
    this.http.patch<Todo>(`http://localhost:3000/todos/${todo.id}`, {
      // ...todo,
      completed: !todo.completed
    })
      .subscribe(() => {
        this.todos.update(todos => {
          return todos.map(t => {
            if (t.id === todo.id) {
              return { ...t, completed: !t.completed}
            }
            return t;
          })
        })
      })


  }


}
interface Todo {
  id: number;
  title: string;
  completed: boolean;
}
