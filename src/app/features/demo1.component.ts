import { NgComponentOutlet } from '@angular/common';
import { Component, inject, Type, ViewContainerRef } from '@angular/core';
import { load } from 'mime';
import { BoxComponent } from '../shared/box.component';
import { Child1Component } from '../shared/components/child1.component';
import { Child2Component } from '../shared/components/child2.component';
import { PanelComponent } from '../shared/panel.component';
import { SharedModule } from '../shared/shared.module';
import { WidgetComponent } from '../shared/widget.component';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    SharedModule,
    NgComponentOutlet,
    Child1Component,
    Child2Component
  ],
  template: `
    <p>
      demo1 works!
    </p>
    
    <app-child1 />
    <app-child2 />
    
      <ng-template *ngComponentOutlet="template.component; inputs: template.props"></ng-template>

    
    <button (click)="loadOne()">1</button>
    <button (click)="loadTwo()">2</button>
  `,
  styles: ``
})
export default class Demo1Component {
  view = inject(ViewContainerRef)
  template: LoaderConfig = {
    component: PanelComponent,
    props: null
  };

  ngOnInit() {

    const ref = this.view.createComponent(BoxComponent);
    // ref.instance.variant = 'warn'
    ref.setInput('variant', 'warn')
  }

  loadOne() {
    this.template = {
      component: WidgetComponent,
      props: null
    }
  }
  loadTwo() {
    this.template = {
      component: BoxComponent,
      props: {
        variant: 'warn',
        size: 'full'
      }
    }
  }

  protected readonly load = load;
}
export type LoaderConfig = {
  component: Type<any>,
  props: any;
}

const map = {
  'box': BoxComponent,
  'widget': WidgetComponent
}
