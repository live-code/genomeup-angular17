import { AsyncPipe } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <p>
      product works!
    </p>
    <h1>Id: {{productId$ | async}}</h1>
    <h1>Data: {{title }} </h1>
  `,
  styles: ``
})
export default class ProductComponent {
  @Input() set productId(val: string) {
    this.productId$.next(val)
  };
  @Input() title: string | undefined;

  productId$ = new BehaviorSubject<string | null>(null);
}
