import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo11',
  standalone: true,
  imports: [],
  template: `
    
    <h1>switch</h1>

    @switch (currentStep()) {
        @case ('step1') {
            <h2>Step1</h2>
            <button (click)="currentStep.set('step2')">NEXT</button>
        }
        @case ('step2') {
            <h2>Step2</h2>
            <button (click)="currentStep.set('step3')">NEXT</button>
        }
        @case ('step3') {
            <h2>Basta!</h2>
        }
        @default {
            Welcome to Wizard
            
            <button (click)="currentStep.set('step1')">NEXT</button>
        }
    }
  `,
  styles: ``
})
export default class Demo11Component {
  currentStep = signal<'step1' | 'step2' | 'step3' | null>(null);
}
