import { AsyncPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { toObservable } from '@angular/core/rxjs-interop';
import { catchError, debounce, debounceTime, filter, of, switchMap } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-demo9',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
      <input
              type="text"
              (input)="onChangeText($event)"
      >

      {{ citySignal() }}
      {{ (meteo$ | async)?.main?.temp }}

  `,
  styles: ``
})
export default class Demo9Component {
  citySignal = signal('')
  http = inject(HttpClient)

  meteo$ = toObservable(this.citySignal)
    .pipe(
      filter(text => text.length > 2),
      debounceTime(1000),
      switchMap(
        text => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .pipe(
            catchError(() => of(null))
          )
      )
    )

  onChangeText(event: Event) {
    const input = event.currentTarget as HTMLInputElement
    this.citySignal.set(input.value)
  }


}
