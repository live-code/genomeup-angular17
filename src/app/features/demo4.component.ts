import { Component } from '@angular/core';
import { BaseService } from '../shared/services/base.service';
import { UserService } from '../shared/services/user.service';
import { WidgetComponent } from '../shared/widget.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    WidgetComponent
  ],
  template: `
    <p>
      demo4 works!
    </p>
    
    <app-widget  />
    <app-widget  />
    <app-widget  />
    
  `,
  providers: [
    { provide: BaseService, useClass: UserService}
  ],
  styles: ``
})
export default class Demo4Component {

}
