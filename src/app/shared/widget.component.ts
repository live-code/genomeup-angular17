import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { HttpClient } from '@angular/common/http';
import { Component, inject, Input } from '@angular/core';
import { PanelComponent } from './panel.component';
import { BaseService } from './services/base.service';
import { UserService } from './services/user.service';
import { WidgetService } from './services/widget.service';

@Component({
  selector: 'app-widget',
  standalone: true,
  imports: [
    PanelComponent
  ],
  template: `
    <p (click)="widgetSrv.title = 'pippo'">
      widget works! {{widgetSrv.title}}
    </p>
    <button (click)="baseSrv.load()">load</button>
    
    <app-panel />
  `,
  providers: [
    WidgetService
  ],
  styles: ``
})
export class WidgetComponent {
  baseSrv = inject(BaseService)
  widgetSrv = inject(WidgetService, { skipSelf: true })
  //widgetSrv = inject(WidgetService, { self: true, skipSelf: true, optional: true})

}
