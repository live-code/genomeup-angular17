import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  http = inject(HttpClient)

  load() {
    this.http.get('https://jsonplaceholder.typicode.com/users')
      .subscribe(console.log)
  }
}
