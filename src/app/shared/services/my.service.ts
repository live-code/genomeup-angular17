import { Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyService {
  value = signal(123)
}
