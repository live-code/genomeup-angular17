import { CommonModule } from '@angular/common';
import { booleanAttribute, Component, Input, numberAttribute } from '@angular/core';

@Component({
  selector: 'app-box',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div
      [ngClass]="{
        'w-32 h-32': size === 'sm',
        'w-64 h-64': size === 'xl',
        'w-full h-32': size === 'full',
        'bg-sky-400': variant === 'primary',
        'bg-red-400': variant === 'warn',
      }"
    >
        @if(showTitle) {
          <h1>{{title}}</h1>
        }
        
        {{ content }}
        
        <!--{{value}}-->
        
        <!--{{items | json}}-->
    </div>
  `,
  styles: ``
})
export class BoxComponent {
  @Input() size: 'sm' | 'xl' | 'full' = 'sm'
  @Input() variant: 'primary' | 'warn' = 'primary'
  @Input({ alias: 'body'}) content = ''
  @Input({ required: true }) title = ''
  @Input({ transform: (val: number) => {
    return val * 100
  }}) value: number = 0

  @Input({ transform: (items: string[]) => {
    return items.map(item => item.length)
  }}) items: number[] = []

  @Input({ transform: numberAttribute})
  zoom: number = 0;

  @Input({ transform: booleanAttribute})
  showTitle: boolean = false
}
