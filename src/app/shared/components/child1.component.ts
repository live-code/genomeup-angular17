import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MyService } from '../services/my.service';

@Component({
  selector: 'app-child1',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [],
  template: `
    <p (click)="srv.value.set(999)">
      child1 works!
      {{srv.value()}}
    </p>
  `,
  styles: ``
})
export class Child1Component {
  srv = inject(MyService)
}
