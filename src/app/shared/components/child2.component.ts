import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MyService } from '../services/my.service';

@Component({
  selector: 'app-child2',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `
      <p (click)="srv.value.set(555)">
        child2 works!
        {{srv.value()}}
    </p>
  `,
  styles: ``
})
export class Child2Component {
  srv = inject(MyService)

}
